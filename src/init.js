import globals from "aktivitet/globals";
import store from './store.js';
import loader from './loader.js';

const default_events = [
    'started', 'completed', 'unlocked',
];

export default function ( id, win, events = null ) {
    var api = store.find_api( win );

    if ( api )
        globals.scorm_store = new store( id, api );
    else
        throw "SCORM API not found";

    for ( var evi in default_events )
    {
        globals.$event_root.addEventListener( default_events[ evi ], globals.scorm_store.handle_state_ev.bind( globals.scorm_store ) );
    }

    globals.loader = loader;
};
