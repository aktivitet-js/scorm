class ScormStore
{
    static scan_api( win, max_tries = null )
    {
        var tries = 0;

        if ( max_tries === null )
            max_tries = 500;

        while ( ( win.API == null ) && ( win.parent != null ) && ( win.parent != win ) )
        {
            tries++;

            if ( tries > max_tries )
                return null;
            
            win = win.parent;
        }

        return win.API;
    }

    static find_api( win, max_tries = null )
    {
        var api;

        // try parent
        if ( ( win.parent != null ) && ( win.parent != win ) )
            api = this.scan_api( win.parent, max_tries );
        // try opener
        if ( ( api == null ) && ( win.opener != null ) )
            api = this.scan_api( win.opener, max_tries );

        return api;
    }

    constructor( id, api )
    {
        this.id = id;
        this.api = api;
        this.map = new Map();
        this.save_timeout = null;

        this.api.LMSInitialize();

        this.load();
    }

    get_aid( $activity )
    {
        return $activity.dataset.aid || $activity.id || null;
    }

    load()
    {
        var states;

        states = this.api.LMSGetValue( 'cmi.suspend_data' );

        if ( states )
        {
            states = JSON.parse( states );
            this.map = new Map( Object.entries( states ) );
        }

    }

    #save()
    {
        var states = Object.fromEntries( this.map.entries() );

        this.api.LMSSetValue( 'cmi.suspend_data', JSON.stringify( states ) );
        this.api.LMSCommit();
    }

    save()
    {
        if ( this.save_timeout )
            clearTimeout( this.save_timeout );

        this.save_timeout = setTimeout( this.#save.bind( this ), 1000 );
    }

    handle_state_ev( e )
    {
        var aid = this.get_aid( e.target );
        // require id
        if ( ! aid )
            return;

        var state = e.target.activity.save();

        this.set( aid, state );
    }

    set( k, v )
    {
        this.map.set( k, v );
        this.save();
    }

    get( k )
    {
        return this.map.get( k );
    }

    has( k )
    {
        return this.map.has( k );
    }
}

export default ScormStore;
