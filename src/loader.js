import globals from "aktivitet/globals";

export default function loader( $activity, options ) {
    var aid = globals.scorm_store.get_aid( $activity );

    if ( aid && globals.scorm_store.has( aid ) )
    {
        return globals.scorm_store.get( aid );
    }
    return {};
};